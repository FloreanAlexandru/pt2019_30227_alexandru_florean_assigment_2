package Supermarket;
import java.util.*;

public class SimulationManager implements Runnable {

	public int max_timp_procesare;
	public int min_timp_procesare;
	public int timp_limita;
	public int max_timp_sosire;
	public int min_timp_sosire;
	public int nr_operator;
	public int nr_task;
	private Interfata a1;
	private Thread thr;
	public int timp_curent =0;	
	
	
	private ArrayList<Task> clienti;
	private Planificare planificare;
	//private Interfata interfata;
	
	public SimulationManager(Interfata a1) throws InterruptedException {
		//System.out.println("a");
		this.a1=a1;
		
		if(ViewTest.apasat==true) {
			max_timp_procesare=Integer.parseInt(a1.get1());
			min_timp_procesare=Integer.parseInt(a1.get2());
			timp_limita=Integer.parseInt(a1.get4());
			nr_operator=Integer.parseInt(a1.get3());
			nr_task=Integer.parseInt(a1.get5());
			max_timp_sosire=Integer.parseInt(a1.geta());
			min_timp_sosire=Integer.parseInt(a1.getb());
			
		//System.out.println("b");
		//System.out.println(timp_limita +"");
		}
		clienti = new ArrayList<Task> (nr_task);
		planificare = new Planificare(100, nr_operator,a1);
		generare_N_Tasks();
	
  }	
 
	private void generare_N_Tasks() {
	//	int v1[] = null;
	//	int ok=0;
		
		int i;
		Random rand= new Random();
		Task client= new Task(0,0,0);
		
		for(i=0;i<nr_task;i++) {
		
		client= new Task(0,0,0);
		client.setNr_id(rand.nextInt((10000)));
		client.setTimp_sosire(rand.nextInt((max_timp_sosire-min_timp_sosire)+1)+min_timp_sosire);
		client.setTimp_procesare(rand.nextInt((max_timp_procesare-min_timp_procesare)+1)+min_timp_procesare);
		
		//System.out.println("Id"+client.getNr_id());
		
		clienti.add(i,client);
		}
		Collections.sort(clienti);
		
		//for(Task j : clienti)
			//System.out.println(j);
			//System.out.println(clienti.get(i).getNr_id()+" "+clienti.get(i).getTimp_sosire()+"\n");
}


	public void run() {
	
			//System.out.println(timp_limita+"treaba ");
		while(timp_curent < timp_limita ) {
			a1.setText1(a1.getText1()+"Timpul curent "+timp_curent+"\n");
			for(Task i:clienti) {
				//System.out.println(i+" clientul ");
				if(i.getTimp_sosire()==timp_curent) {
					try {
						planificare.Imprastiere(i);
						
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
			
			
			timp_curent++;
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}

	}
}
