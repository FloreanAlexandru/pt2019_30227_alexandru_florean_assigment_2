package Supermarket;
import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.Random;
import java.lang.String;

public class OperatorCasa implements Runnable{
	private BlockingQueue <Task> taskuri;
	private AtomicInteger waitingPeriod;
	private Thread thr;
	private int id;
	private Interfata a1;
	public int nr=1;
	public static String s1="",s2="",s3="",s4="",s5="",s6="",s7="",s8="";
	public OperatorCasa(int id,Interfata a1) {
		taskuri=new ArrayBlockingQueue<Task>(4096);
		this.id=id;
		this.a1=a1;
		waitingPeriod =new AtomicInteger(0);
		//thr =new Thread(this);
		//thr.start();
	}
	
	public void subTask() throws InterruptedException{
	Task t;
	int a=0;
	for(int i=1;i<=waitingPeriod.intValue();i++) {
		a++;
	}
	t=taskuri.peek();
	this.waitingPeriod.set(a-t.getTimp_procesare());
	
	//System.out.println(t.getTimp_procesare()+"AICI");
	
	Thread.sleep(1000*(t.getTimp_procesare()+1));
 	a1.setText1(a1.getText1()+t.toString()+" "+(this.id+1)+"\n");

	if(this.id==0) {
		for(int i=1;i<s1.length();i++) {
			if(Character.isWhitespace(s1.charAt(i))) {
				//System.out.println(s1+"inainte");
				s1=(s1.substring(i+1));
				//System.out.println(s1+"dupa");
				a1.set1("");
				a1.set1(s1);
				break;
		}
	}}
				
		 if(this.id==1) {
			for(int i=1;i<s2.length();i++) {
				if(Character.isWhitespace(s2.charAt(i))) {
					s2=(s2.substring(i+1));
					a1.set2("");
					a1.set2(s2);
					break;
			}
		}
		 }				
		 if(this.id==2){
			for(int i=1;i<s3.length();i++) {
				if(Character.isWhitespace(s3.charAt(i))) {
					s3=(s3.substring(i+1));
					a1.set3("");
					a1.set3(s3);
					break;
			}
		}
		 }	
		 if(this.id==3){
			for(int i=1;i<s4.length();i++) {
				if(Character.isWhitespace(s4.charAt(i))) {
					s4=(s4.substring(i+1));
					a1.set4("");
					a1.set4(s4);
					break;
			}
		}
		 }		
		
		 if(this.id==4){
			for(int i=1;i<s5.length();i++) {
				if(Character.isWhitespace(s5.charAt(i))) {
					s5=(s5.substring(i+1));
					a1.set5("");
					a1.set5(s5);
					break;
			}
		}
		 }		
		if(this.id==5){
			for(int i=1;i<s6.length();i++) {
				if(Character.isWhitespace(s6.charAt(i))) {
					s6=(s6.substring(i+1));
					a1.set6("");
					a1.setText1(s6);
					break;
			}
		}
		}				
		 if(this.id==6){
			for(int i=1;i<s7.length();i++) {
				if(Character.isWhitespace(s7.charAt(i))) {
					s7=(s7.substring(i+1));
					a1.set7("");
					a1.setText1(s7);
					break;
			}
		}
		 }		
		 if(this.id==7){
			for(int i=1;i<s8.length();i++) {
				if(Character.isWhitespace(s8.charAt(i))) {
					s8=(s8.substring(i+1));
					a1.set8("");
					a1.setText1(s8);
					break;
			}
		}
	}		
		
	
	taskuri.remove(t);
	
	//System.out.println("Timp procesare "+t.getTimp_procesare());
	//System.out.println(t.toString()+""+id);
	//System.out.println(t.toString()+" "+this.id);
} 
	
	
	public void addTask(Task new_Task) throws InterruptedException {	
		taskuri.put(new_Task);
	//	System.out.println(new_Task.toString1()+" "+this.id);
		
		a1.setText1(a1.getText1()+new_Task.toString1()+" "+(this.id+1)+"\n");
		if(this.id==0) {
			s1="";
			s1=s1+a1.get6();
			a1.set1(a1.get6()+"Task->"+new_Task.getNr_id()+" ");
			
			}else if(this.id==1) {
				s2="";	
				s2=s2+a1.get7();
				a1.set2(a1.get7()+"Task->"+new_Task.getNr_id()+" ");
				
				
			}else if(this.id==2){
				s3="";
				s3=s3+a1.get8();
				a1.set3(a1.get8()+"Task->"+new_Task.getNr_id()+" ");
				
				
			}else if(this.id==3){
				s4="";
				s4=s4+a1.get9();
				a1.set4(a1.get9()+"Task->"+new_Task.getNr_id()+" ");
				
			}else if(this.id==4){
				s5="";
				s5=s5+a1.get10();
				a1.set5(a1.get10()+"Task->"+new_Task.getNr_id()+" ");
				
				
			}else if(this.id==5) {
				s6="";
				s6=s6+a1.get11();
				a1.set6(a1.get11()+"Task->"+new_Task.getNr_id()+" ");
				
			}else if(this.id==6){
				s7="";
				s7=s7+a1.get12();
				a1.set7(a1.get12()+"Task->"+new_Task.getNr_id()+" ");
				
			}else if(this.id==7){
				s8="";
				s8=s8+a1.get13();
				a1.set8(a1.get13()+"Task->"+new_Task.getNr_id()+" ");
				
			}
		
		this.waitingPeriod.addAndGet(new_Task.getTimp_procesare());
	
	}

	public Thread getThr() {
		return thr;
	}


	public void setThr(Thread thr) {
		this.thr = thr;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}

	
	public BlockingQueue<Task> getTaskuri() {
		return taskuri;
	}

	public void setTaskuri(BlockingQueue<Task> taskuri) {
		this.taskuri = taskuri;
	}

	public AtomicInteger getWaitingPeriod() {
		return waitingPeriod;
	}

	public void setWaitingPeriod(AtomicInteger waitingPeriod) {
		this.waitingPeriod = waitingPeriod;
	}

	
	public void run() {
		boolean ok = true;
		int timp=0;
		while (ok) {
			if(taskuri.isEmpty()!=true) {
			
				try {
					subTask();
					//Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
}

