package Supermarket;
import java.util.*;

public class Planificare {
	private ArrayList<OperatorCasa> Operatori;
	private int maxTaskPerOperator;
	private int maxNrServere;
	private Interfata a1;

	public Planificare(int maxTaskPerOperator,int maxNrServere,Interfata a1) {
		this.maxNrServere=maxNrServere;
		this.maxTaskPerOperator=maxTaskPerOperator;
		Operatori=new ArrayList<OperatorCasa> (maxNrServere);
		
		for(int i=0;i<maxNrServere;i++) {
			OperatorCasa casa=new OperatorCasa(i,a1);
			Thread t=new Thread(casa);
			t.start();
			Operatori.add(i,casa);
		}
		
	}
	
	public void Imprastiere (Task t) throws InterruptedException {
		
		int min = 9999; // nr de task-uri imposibil in viata reala
		for(OperatorCasa i : Operatori) {
			if(i.getTaskuri().size() < min) {
				min=i.getTaskuri().size();
			}
		}
		
		for(OperatorCasa i: Operatori) {
			//System.out.println(i.getTaskuri().size());
			if(i.getTaskuri().size() == min) {
				i.addTask(t);
				break;
			}
		}
		
	}

	public List<OperatorCasa> getOperatori() {
		return Operatori;
	}

	public void setOperatori(ArrayList<OperatorCasa> operatori) {
		Operatori = operatori;
	}

	public int getMaxTaskPerOperator() {
		return maxTaskPerOperator;
	}

	public void setMaxTaskPerOperator(int maxTaskPerOperator) {
		this.maxTaskPerOperator = maxTaskPerOperator;
	}

	public int getMaxNrServere() {
		return maxNrServere;
	}

	public void setMaxNrServere(int maxNrServere) {
		this.maxNrServere = maxNrServere;
	}
	
	
}