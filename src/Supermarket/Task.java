package Supermarket;
import java.util.*;

public class Task implements Comparable {
	
	private int nr_id;
	private int timp_sosire;
	private int timp_procesare;

	
	public Task(int timp_sosire,int timp_procesare,int timp_finalizare) {
		this.nr_id=nr_id;
		this.timp_procesare=timp_procesare;
		this.timp_sosire=timp_sosire;
	}

	public int getTimp_sosire() {
		return timp_sosire;
	}

	public void setTimp_sosire(int timp_sosire) {
		this.timp_sosire = timp_sosire;
	}

	public int getTimp_procesare() {
		return timp_procesare;
	}

	public void setTimp_procesare(int timp_procesare) {
		this.timp_procesare = timp_procesare;
	}
	
	public int getNr_id() {
		return nr_id;
	}

	public void setNr_id(int nr_id) {
		this.nr_id = nr_id;
	}

	public int compareTo(Object o) {
		
		if(this.timp_sosire <((Task)o).timp_sosire) {
			return -1;
		}else if(this.timp_sosire == ((Task)o).timp_sosire) {
			return 0;
		}else {
			return 1;
		}
	}
	
	public String toString() {
		return "Clientul cu id "+ nr_id+" cu timpul de procesare "+ timp_procesare + " si cu timpul de sosire " + timp_sosire+ " a plecat de la casa ";
	}
	
	public String toString1() {
		return "Clientul cu id " + nr_id+" cu timpul de procesare "+ timp_procesare + " si cu timpul de sosire "+ timp_sosire+" a sosit la casa ";
	}
	
	
}
