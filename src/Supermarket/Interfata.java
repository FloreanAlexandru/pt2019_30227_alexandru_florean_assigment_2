package Supermarket;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Interfata extends JFrame{

	private JLabel Queue1=new JLabel("Queue1");
	private JTextField one=new JTextField(50);
	private JLabel Queue2=new JLabel("Queue2");
	private JTextField two=new JTextField(50);
	private JLabel Queue3=new JLabel("Queue3");
	private JTextField three=new JTextField(50);
	private JLabel Queue4=new JLabel("Queue4");
	private JTextField four=new JTextField(50);
	private JLabel Queue5=new JLabel("Queue5");
	private JTextField five=new JTextField(50);
	private JLabel Queue6=new JLabel("Queue6");
	private JTextField six=new JTextField(50);
	private JLabel Queue7=new JLabel("Queue7");
	private JTextField seven=new JTextField(50);
	private JLabel Queue8=new JLabel("Queue8");
	private JTextField eight=new JTextField(50);
	
	private JLabel t1=new JLabel("Timpul maxim de procesare");
	private JTextField a1 =new JTextField(5);
	private JLabel t2=new JLabel("Timpul minim de procesare");
	private JTextField a2 =new JTextField(5);
	private JLabel t3=new JLabel("Numar de Case                    ");
	private JTextField a3=new JTextField(5);
	private JLabel t4=new JLabel("Timp limita                            ");
	private JTextField a4=new JTextField(5);
	private JLabel t5=new JLabel("Numar taskuri                      ");
	private JTextField a5=new JTextField(5);
	private JLabel t6=new JLabel("Timpul maxim de sosire     ");
	private JTextField a6=new JTextField(5);
	private JLabel t7=new JLabel("Timpul minim de sosire      ");
	private JTextField a7=new JTextField(5);
	
	private JLabel x = new JLabel("Afisari");
	private JTextArea y=new JTextArea(10000,10000);
	
	private JButton buton1 = new JButton("Start");
	private JButton buton2 = new JButton("Stop");
	private JButton buton3 = new JButton("Resume");
	
	public Interfata() {
		//JFrame frame = new JFrame("Supermarket");
		//..frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//frame.setSize(750,750);
		
		this.setSize(750,750);
		JPanel c1=new JPanel();
		c1.add(Queue1);
		c1.add(one);
		c1.setLayout(new FlowLayout());
		
		JPanel c2=new JPanel();
		c2.add(Queue2);
		c2.add(two);
		c2.setLayout(new FlowLayout());
		
		JPanel c3=new JPanel();
		c3.add(Queue3);
		c3.add(three);
		c3.setLayout(new FlowLayout());
		
		JPanel c4=new JPanel();
		c4.add(Queue4);
		c4.add(four);
		c4.setLayout(new FlowLayout());
		
		JPanel c5=new JPanel();
		c5.add(Queue5);
		c5.add(five);
		c5.setLayout(new FlowLayout());

		JPanel c16 =new JPanel();
		c16.add(Queue6);
		c16.add(six);
		c16.setLayout(new FlowLayout());
		
		JPanel c17 =new JPanel();
		c17.add(Queue7);
		c17.add(seven);
		c17.setLayout(new FlowLayout());
		
		JPanel c18 =new JPanel();
		c18.add(Queue8);
		c18.add(eight);
		c18.setLayout(new FlowLayout());
		
		JPanel c6=new JPanel();
		c6.add(c1);
		c6.add(c2);
		c6.add(c3);
		c6.add(c4);
		c6.add(c5);
		c6.add(c16);
		c6.add(c17);
		c6.add(c18);
		c6.setLayout(new BoxLayout(c6,BoxLayout.Y_AXIS));
		
		JPanel c7=new JPanel();
		c7.add(t1);
		c7.add(a1);
		c7.setLayout(new FlowLayout());
		
		JPanel c8=new JPanel();
		c8.add(t2);
		c8.add(a2);
		c8.setLayout(new FlowLayout());
		
		JPanel c9=new JPanel();
		c9.add(t3);
		c9.add(a3);
		c9.setLayout(new FlowLayout());
		
		JPanel c10=new JPanel();
		c10.add(t4);
		c10.add(a4);
		c10.setLayout(new FlowLayout());
		
		JPanel c11=new JPanel();
		c11.add(t5);
		c11.add(a5);
		c11.setLayout(new FlowLayout());
		
		JPanel c19=new JPanel();
		c19.add(t6);
		c19.add(a6);
		c19.setLayout(new FlowLayout());
		
		JPanel c20=new JPanel();
		c20.add(t7);
		c20.add(a7);
		c20.setLayout(new FlowLayout());
		
		JPanel c12= new JPanel();
		c12.add(c7);
		c12.add(c8);
		c12.add(c9);
		c12.add(c10);
		c12.add(c11);
		c12.add(c19);
		c12.add(c20);
		
		JPanel c=new JPanel();
		c.add(buton1);
		c.setLayout(new FlowLayout());
		
		c12.add(c);
		c12.setLayout(new BoxLayout(c12,BoxLayout.Y_AXIS));
		
		JPanel c13=new JPanel();
		c13.add(c12);
		c13.add(c6);
		c13.setLayout(new BoxLayout(c13,BoxLayout.Y_AXIS));
		
		JPanel c14=new JPanel();
		c14.add(x);
		c14.add(y);
		c14.setLayout(new FlowLayout());
		JScrollPane jsp = new JScrollPane(y,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		
		
		JPanel c15=new JPanel();
		this.setContentPane(c15);

		c15.add(c13);
		c15.add(c14);
		c15.add(jsp);
		c15.setLayout(new BoxLayout(c15,BoxLayout.Y_AXIS));
		c15.setVisible(true);
		this.setVisible(true);
		
	}
	
	public void butonStart(ActionListener a) {
		buton1.addActionListener(a);
	}
	
	public JButton getButon1() {
		return buton1;
	}

	public void setButon1(JButton buton1) {
		this.buton1 = buton1;
	}

	public void butonStop(ActionListener a) {
		buton2.addActionListener(a);
	}
	
	public void butonResume(ActionListener a) {
		buton3.addActionListener(a);
	}
	
	public String get1() {
		return a1.getText();
	}
	
	public String get2() {
		return a2.getText();
	}
	
	public String get3() {
		return a3.getText();
	}
	
	public String get4() {
		return a4.getText();
	}
	public String get5() {
		return a5.getText();
	}
	
	public String geta() {
		return a6.getText();
	}
	
	public String getb() {
		return a7.getText();
	}

	public String get6() {
		return one.getText();
	}
	
	public String get7() {
		return two.getText();
	}
	
	public String get8() {
		return three.getText();
	}
	
	public String get9() {
		return four.getText();
	}
	
	public String get10() {
		return five.getText();
	}
	
	public String get11() {
		return six.getText();
	}
	
	public String get12() {
		return seven.getText();
	}
	
	public String get13() {
		return eight.getText();
	}
	
	public String getText1() {
		return y.getText();
	}
	
	public void set1(String a) {
		one.setText(a);
	}
	
	public void set2(String a) {
		two.setText(a);	
	}
	
	public void set3(String a) {
		three.setText(a);
	}
	
	public void set4(String a) {
		four.setText(a);
	}
	
	public void set5(String a) {
		five.setText(a);
	}
	
	public void set6(String a) {
		six.setText(a);
	}
	
	public void set7(String a) {
		seven.setText(a);
	}
	
	public void set8(String a) {
		eight.setText(a);
	}
	
	public void setText1(String a) {
		y.setText(a);
	}

}